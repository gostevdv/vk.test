﻿using System.Collections.Generic;

namespace VK.Services.Constants
{
    /// <summary>
    /// Класс констант и правил
    /// </summary>
    public static class Const
    {
        public static string Numbers => "1234567890";
        public static string OperatorsFirstPriority => "*/";
        public static string OperatorsSecondPriority => "+-";
        public static string Operators => $"{OperatorsFirstPriority}{OperatorsSecondPriority}";
        public static string Brackets => "()";

        /// <summary>
        /// Список доступных символов в выражение
        /// </summary>
        public static string WhiteListSymbols => $"{Numbers}{Operators}{Brackets}";

        /// <summary>
        /// Тут будут правила проверки следующего символа. Ключ - это текущий символ, значение - строка возможных символов в следующей позиции
        /// </summary>
        public static Dictionary<char, string> WhiteListNextSymbols => new Dictionary<char, string>
        {
            ['0'] = $"{Numbers}{Operators})",
            ['1'] = $"{Numbers}{Operators})",
            ['2'] = $"{Numbers}{Operators})",
            ['3'] = $"{Numbers}{Operators})",
            ['4'] = $"{Numbers}{Operators})",
            ['5'] = $"{Numbers}{Operators})",
            ['6'] = $"{Numbers}{Operators})",
            ['7'] = $"{Numbers}{Operators})",
            ['8'] = $"{Numbers}{Operators})",
            ['9'] = $"{Numbers}{Operators})",
            ['-'] = $"{Numbers}{OperatorsSecondPriority}(",
            ['+'] = $"{Numbers}{OperatorsSecondPriority}(",
            ['*'] = $"{Numbers}{OperatorsSecondPriority}(",
            ['/'] = $"{Numbers}{OperatorsSecondPriority}(",
            ['('] = $"{Numbers}{OperatorsSecondPriority}(",
            [')'] = $"{Operators})"
        };

        /// <summary>
        /// Запрешенные стартовые символы
        /// </summary>
        public static string BlackListStartSign => $"{OperatorsFirstPriority})";


        /// <summary>
        /// Запрешенные последние символы
        /// </summary>
        public static string BlackListLastSign => $"{Operators}(";

        /// <summary>
        /// Словарь математических преоразований
        /// </summary>
        public static Dictionary<string, string> MathMapConvert => new Dictionary<string, string>
        {
            ["-+"] = "-",
            ["+-"] = "-",
            ["++"] = "+",
            ["--"] = "+",
        };
    }
}
