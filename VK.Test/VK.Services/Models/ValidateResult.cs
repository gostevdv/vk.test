﻿namespace VK.Services.Models
{
    public class ValidateResult
    {
        /// <summary>
        /// Результат валидации, по дефолту true
        /// </summary>
        public bool Result { get; set; } = true;

        /// <summary>
        /// Соообщение об ошибке, если не удачная валидация
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
