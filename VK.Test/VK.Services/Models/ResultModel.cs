﻿using VK.Services.Enums;

namespace VK.Services.Models
{
    public class ResultModel
    {
        public string Answer { get; set; }
        public ResultStatus ResultStatusCode { get; set; }
        public string ResultStatus => ResultStatusCode.ToString("G");
        public string ErrorMessage { get; set; }
    }
}
