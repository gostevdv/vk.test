﻿namespace VK.Services.Interfaces.NodeExpression
{
    /// <summary>
    /// Элемент отвечает за числа
    /// </summary>
    public interface INumberNodeExpression : INodeExpression
    {

    }
}