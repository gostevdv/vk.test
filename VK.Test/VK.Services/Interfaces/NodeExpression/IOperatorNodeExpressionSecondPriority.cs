﻿namespace VK.Services.Interfaces.NodeExpression
{
    /// <summary>
    /// Операторы втрого приоритета + -
    /// </summary>
    public interface IOperatorNodeExpressionSecondPriority : IOperatorNodeExpression
    {

    }
}