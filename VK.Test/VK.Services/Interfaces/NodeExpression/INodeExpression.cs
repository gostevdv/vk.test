﻿namespace VK.Services.Interfaces.NodeExpression
{
    /// <summary>
    /// обобщенный элемент моих выражений
    /// </summary>
    public interface INodeExpression
    {
        /// <summary>
        /// Получить результат Node
        /// </summary>
        /// <returns></returns>
        public double Result();
    }
}
