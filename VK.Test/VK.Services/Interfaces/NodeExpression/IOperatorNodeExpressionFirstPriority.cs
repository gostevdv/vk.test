﻿namespace VK.Services.Interfaces.NodeExpression
{
    /// <summary>
    /// Операторы перовго приоритета * /
    /// </summary>
    public interface IOperatorNodeExpressionFirstPriority : IOperatorNodeExpression
    {

    }
}