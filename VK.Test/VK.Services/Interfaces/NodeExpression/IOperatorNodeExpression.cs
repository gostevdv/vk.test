﻿namespace VK.Services.Interfaces.NodeExpression
{
    /// <summary>
    /// Элемент отвечает за операторы
    /// </summary>
    public interface IOperatorNodeExpression : INodeExpression
    {
        void SetPairNodes(INodeExpression leftNode, INodeExpression rightNode);
    }
}