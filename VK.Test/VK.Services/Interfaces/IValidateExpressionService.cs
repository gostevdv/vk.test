﻿using VK.Services.Models;

namespace VK.Services.Interfaces
{
    public interface IValidateExpressionService
    {
        /// <summary>
        /// Проверка на пустоту
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        ValidateResult CheckNoEmpty(string expression);

        /// <summary>
        /// Проверка на макс длину. Небольше 10, потому что если больше, то не влезет в double
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        ValidateResult CheckMaxLen(string expression);

        /// <summary>
        /// Проверка на корректные символы
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        ValidateResult CheckCorrectSymbols(string expression);

        /// <summary>
        /// Проверка на корректность использования самих символов друг с другом(проверяю чтобы выржаение всётаки имело математический вид и единый стиль)
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        ValidateResult CheckOrderSignsAndNumbers(string expression);

        /// <summary>
        /// Проверка на корректность последовательность скобок
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        ValidateResult CheckSequenceOfBrackets(string expression);

        /// <summary>
        /// Проверка на первый символ, чтобы не было операторов кроме + и -
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        ValidateResult CheckStartSign(string expression);

        /// <summary>
        /// Проверка на последний символ, чтобы не было операторов
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        ValidateResult CheckLastSign(string expression);

        /// <summary>
        /// Проверка на несколько оперторов подряд +-*/(не больше 2х)
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        ValidateResult CheckConsecutiveOperators(string expression);
    }
}