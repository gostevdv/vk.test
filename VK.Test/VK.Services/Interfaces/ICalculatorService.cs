﻿using System.Threading.Tasks;
using VK.Services.Models;

namespace VK.Services.Interfaces
{
    public interface ICalculatorService
    {
        /// <summary>
        /// Получить результат выражения
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<ResultModel> Calculate(string expression);
    }
}