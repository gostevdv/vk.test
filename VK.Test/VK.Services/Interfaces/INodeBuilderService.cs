﻿using System.Collections.Generic;
using VK.Services.Interfaces.NodeExpression;

namespace VK.Services.Interfaces
{
    public interface INodeBuilderService
    {
        /// <summary>
        /// Строю дерево узлов выражения
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        INodeExpression BuildNodeTreeExpression(List<string> expression);
    }
}