﻿using System.Collections.Generic;

namespace VK.Services.Interfaces
{
    public interface INormalizeExpressionService
    {
        /// <summary>
        /// Парсинг элементов выражения в массив
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        List<string> ParseExpressionToArray(string expression);

        /// <summary>
        /// Получить нормализованное выражение(все отрицательные значения поместить в скобки и добавить как будто вычитание из 0)
        /// </summary>
        /// <returns></returns>
        public List<string> GetNormalizeExpressionElements(List<string> expression);
    }
}