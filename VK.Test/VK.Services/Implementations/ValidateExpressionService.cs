﻿using System;
using System.Collections.Generic;
using System.Linq;
using VK.Services.Constants;
using VK.Services.Interfaces;
using VK.Services.Models;

namespace VK.Services.Implementations
{
    /// <summary>
    /// Сервис проверки выражения на валидность
    /// </summary>
    public class ValidateExpressionService : IValidateExpressionService
    {
        private readonly string _whiteListSymbols;
        private readonly Dictionary<char, string> _whiteListNextSymbols;

        public ValidateExpressionService()
        {
            _whiteListSymbols = Const.WhiteListSymbols;
            _whiteListNextSymbols = Const.WhiteListNextSymbols;
        }


        /// <summary>
        /// Обобщенный метод валидации, предикат на негативный исход
        /// </summary>
        /// <param name="isNotCorrectExpressionPredicate"></param>
        /// <param name="expression"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        private ValidateResult CheckValidateByPredicate(Predicate<string> isNotCorrectExpressionPredicate, string expression, string errorMessage)
        {
            var result = new ValidateResult();

            if (isNotCorrectExpressionPredicate(expression))
            {
                result.Result = false;
                result.ErrorMessage = errorMessage;
            }

            return result;
        }


        /// <summary>
        /// Проверка на пустоту
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ValidateResult CheckNoEmpty(string expression)
        {
            return CheckValidateByPredicate(string.IsNullOrEmpty, expression, "Введённое выражение пустое");
        }

        /// <summary>
        /// Проверка на макс длину. Небольше 10, потому что если больше, то не влезет в double
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ValidateResult CheckMaxLen(string expression)
        {
            return CheckValidateByPredicate(s => s.Length > 10, expression, "Длина выражения больше 10");
        }

        /// <summary>
        /// Проверка на корректные символы
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ValidateResult CheckCorrectSymbols(string expression)
        {
            var incorrectSymbols = string.Join(", ", expression
                .Where(e => !_whiteListSymbols.Contains(e)).Distinct());

            return CheckValidateByPredicate(s => s.Any(e => !_whiteListSymbols.Contains(e)), expression,
                $"Следующие символы не допустимы в выражение: {incorrectSymbols}");
        }

        /// <summary>
        /// Проверка на корректность использования самих символов друг с другом(проверяю чтобы выржаение всётаки имело математический вид и единый стиль)
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ValidateResult CheckOrderSignsAndNumbers(string expression)
        {
            return CheckValidateByPredicate(IsNotCorrectPairSymbols, expression,
                $"Математическое выражение составлено не верно. Проверьте порядок знаков, цифр и скобочек");
        }

        /// <summary>
        /// Проверяю следующий символ на запрет использоваться после текущего символа для всех символов в выражение
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private bool IsNotCorrectPairSymbols(string expression)
        {
            //если длина выражения меньше 2х то проверять не надо
            if (expression.Length < 2)
                return false;

            for (int i = 0; i < expression.Length - 1; i++)
            {
                var thisChar = expression[i];
                var nextChar = expression[i + 1];

                //Игнорирую неучтенные символы. Валидация упадет в CheckCorrectSymbols
                if (!_whiteListNextSymbols.ContainsKey(thisChar))
                    continue;

                if (!_whiteListNextSymbols[thisChar].Contains(nextChar))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Проверка на корректность последовательность скобок
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ValidateResult CheckSequenceOfBrackets(string expression)
        {
            return CheckValidateByPredicate(IsNotCorrectSequenceOfBrackets, expression,
                $"Не коректное использование скобочек. Проверьте порядок скобочек и их количество.");
        }

        /// <summary>
        /// Проверяю на некорректность скобочную последовательность
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private bool IsNotCorrectSequenceOfBrackets(string expression)
        {
            var stackBrackets = new Stack<char>();

            foreach (var sign in expression)
            {
                if (sign == '(')
                {
                    stackBrackets.Push(sign);
                }

                if (sign == ')')
                {
                    //Если пришла закрывающая а стек пустой, то это не корректно
                    if (stackBrackets.Count == 0)
                        return true;

                    //Вытаскиваю соответстующую скобку из стека
                    stackBrackets.Pop();
                }
            }

            //Если стек не пустой, то это не корректно
            if (stackBrackets.Count != 0)
                return true;

            return false;
        }

        /// <summary>
        /// Проверка на первый символ, чтобы не было операторов кроме + и -
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ValidateResult CheckStartSign(string expression)
        {
            if (string.IsNullOrEmpty(expression))
                return new ValidateResult();

            return CheckValidateByPredicate(e => Const.BlackListStartSign.Contains(e[0]), expression,
                $"Знак {expression[0]} запрещено использовать в начале выражения");
        }

        /// <summary>
        /// Проверка на последний символ, чтобы не было операторов
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ValidateResult CheckLastSign(string expression)
        {
            if (string.IsNullOrEmpty(expression))
                return new ValidateResult();

            return CheckValidateByPredicate(e => Const.BlackListLastSign.Contains(e[^1]), expression,
                $"Знак {expression[^1]} запрещено использовать в конце выражения");
        }

        /// <summary>
        /// Проверка на несколько оперторов подряд +-*/(не больше 2х)
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ValidateResult CheckConsecutiveOperators(string expression)
        {
            return CheckValidateByPredicate(IsConsecutiveOperators, expression,
                $"Нельзя использовать в выражение более 2х подряд идущих операндов");
        }

        /// <summary>
        /// Имеется ли хоть 1 тройка подряд идущих операторов
        /// </summary>
        private bool IsConsecutiveOperators(string expression)
        {
            //Если выражение короче 3х символов то проверять не надо
            if (expression.Length < 3)
                return false;

            for (int i = 1; i < expression.Length - 1; i++)
            {
                var thisElement = expression[i];
                var preElement = expression[i - 1];
                var nextElement = expression[i + 1];
                if (Const.Operators.Contains(thisElement)
                    && Const.Operators.Contains(preElement)
                    && Const.Operators.Contains(nextElement))
                    return true;
            }

            return false;
        }
    }


}