﻿using System;
using VK.Services.Interfaces.NodeExpression;

namespace VK.Services.Implementations.NodeExpression
{
    /// <summary>
    /// Элемент отвечает за /
    /// </summary>
    public class DivideOperatorNodeExpression : IOperatorNodeExpressionFirstPriority
    {
        private INodeExpression _leftNode;
        private INodeExpression _rightNode;

        /// <summary>
        /// Получить результат Node
        /// </summary>
        /// <returns></returns>
        public double Result()
        {
            var rightNode = _rightNode.Result();

            //Добавил эту обработку, потому что он умеет делить на 0, и выдавать бесконечность
            if (rightNode == 0)
                throw new DivideByZeroException();

            //Деление на 0, в CalculatorService добавлю исключение на этот тип ошибки
            return _leftNode.Result() / _rightNode.Result();
        }

        public void SetPairNodes(INodeExpression leftNode, INodeExpression rightNode)
        {
            _leftNode = leftNode ?? new NumberNodeExpression();
            _rightNode = rightNode ?? new NumberNodeExpression();
        }
    }
}