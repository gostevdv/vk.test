﻿using VK.Services.Interfaces.NodeExpression;

namespace VK.Services.Implementations.NodeExpression
{
    /// <summary>
    /// Элемент отвечает за числа
    /// </summary>
    public class NumberNodeExpression: INumberNodeExpression
    {
        private double _number = 0;

        public NumberNodeExpression(double? number = null)
        {
            //тут будут только цифры, все буквы у  нас не прошли валидацию

            _number = number ?? 0;
        }

        /// <summary>
        /// Получить результат Node
        /// </summary>
        /// <returns></returns>
        public double Result()
        {
            return _number;
        }
    }
}
