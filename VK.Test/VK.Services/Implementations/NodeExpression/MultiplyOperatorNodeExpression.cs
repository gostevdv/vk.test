﻿using VK.Services.Interfaces.NodeExpression;

namespace VK.Services.Implementations.NodeExpression
{
    /// <summary>
    /// Элемент отвечает за *
    /// </summary>
    public class MultiplyOperatorNodeExpression : IOperatorNodeExpressionFirstPriority
    {
        private INodeExpression _leftNode;
        private INodeExpression _rightNode;

        /// <summary>
        /// Получить результат Node
        /// </summary>
        /// <returns></returns>
        public double Result()
        {
            return _leftNode.Result() * _rightNode.Result();
        }

        public void SetPairNodes(INodeExpression leftNode, INodeExpression rightNode)
        {
            _leftNode = leftNode ?? new NumberNodeExpression();
            _rightNode = rightNode ?? new NumberNodeExpression();
        }
    }
}