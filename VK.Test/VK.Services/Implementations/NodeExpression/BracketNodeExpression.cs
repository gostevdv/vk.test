﻿using VK.Services.Interfaces.NodeExpression;

namespace VK.Services.Implementations.NodeExpression
{
    /// <summary>
    /// Элемент отвечает за собранное всё выражение или скобку
    /// </summary>
    public class BracketNodeExpression : INodeExpression
    {
        private INodeExpression _nodeExpression;

        public BracketNodeExpression(INodeExpression nodeExpression)
        {
            _nodeExpression = nodeExpression;
        }

        /// <summary>
        /// Получить результат Node
        /// </summary>
        /// <returns></returns>
        public double Result()
        {
            return _nodeExpression.Result();
        }
    }
}