﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VK.Services.Enums;
using VK.Services.Interfaces;
using VK.Services.Interfaces.NodeExpression;
using VK.Services.Models;

namespace VK.Services.Implementations
{
    /// <summary>
    /// Сервис для вычиления выражения
    /// Сразу предусматриваю возможность в будущем использовать асинхронные запросы, поэтому сразу покрываю асинками
    /// </summary>
    public class CalculatorService : ICalculatorService
    {
        private readonly IValidateExpressionService _validateExpressionService;
        private readonly INormalizeExpressionService _normalizeExpressionService;
        private readonly INodeBuilderService _nodeBuilderService;

        public CalculatorService(IValidateExpressionService validateExpressionService, INormalizeExpressionService normalizeExpressionService, INodeBuilderService nodeBuilderService)
        {
            _validateExpressionService = validateExpressionService;
            _normalizeExpressionService = normalizeExpressionService;
            _nodeBuilderService = nodeBuilderService;
        }


        /// <summary>
        /// Получить результат выражения
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<ResultModel> Calculate(string expression)
        {
            expression = expression.Trim(); // очищаю от случайных пробелов в начале и в конце

            //1 этап. Валидация
            var checkValidate = CheckValidateExpression(expression);
            if (!checkValidate.Result)
                return new ResultModel()
                {
                    ErrorMessage = checkValidate.ErrorMessage,
                    ResultStatusCode = ResultStatus.ValidateError
                };

            //2 этап - разбить(спарсить) на составляющие и нормализовать выражение
            var expressionElements = GetNormalizeParseExpression(expression);

            //3 этап - получаю дерево выражения
            var node = GetNodeTreeExpression(expressionElements);

            //4 этап - вычислить результат по дереву выражения
            var result = GetResult(node);

            return result;
        }

        /// <summary>
        /// Получаю результат
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private ResultModel GetResult(INodeExpression node)
        {
            try
            {
                var result = Math.Round(node.Result(), 1).ToString();
                return new ResultModel()
                {
                    ResultStatusCode = ResultStatus.Ok,
                    Answer = result
                };
            }
            catch (DivideByZeroException ex)
            {
                return new ResultModel()
                {
                    ResultStatusCode = ResultStatus.DivideByZeroError,
                    ErrorMessage = "Деление на 0 запрещено"
                }; 
            }
            catch (Exception ex)
            {
                return new ResultModel()
                {
                    ResultStatusCode = ResultStatus.UnknownError,
                    ErrorMessage = $"Прозошла неизвестная ошибка: {ex.Message}"
                };
            }
        }

        /// <summary>
        /// Получаю дерево выражения
        /// </summary>
        /// <param name="expressionElements"></param>
        /// <returns></returns>
        private INodeExpression GetNodeTreeExpression(List<string> expressionElements)
        {
            var node = _nodeBuilderService.BuildNodeTreeExpression(expressionElements);
            return node;
        }

        /// <summary>
        /// Проверяю на валидность выражения
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private ValidateResult CheckValidateExpression(string expression)
        {
            //запускаю всю валидацию, чтобы более полно описать ошибки если будут
            var validationResults = new List<ValidateResult>()
            {
                _validateExpressionService.CheckNoEmpty(expression),
                _validateExpressionService.CheckMaxLen(expression),
                _validateExpressionService.CheckCorrectSymbols(expression),
                _validateExpressionService.CheckOrderSignsAndNumbers(expression),
                _validateExpressionService.CheckSequenceOfBrackets(expression),
                _validateExpressionService.CheckStartSign(expression),
                _validateExpressionService.CheckLastSign(expression),
                _validateExpressionService.CheckConsecutiveOperators(expression),
            };
            
            var totalResult = new ValidateResult()
            {
                ErrorMessage = string.Join("; ", validationResults
                    .Where(v=>!string.IsNullOrEmpty(v.ErrorMessage))
                    .Select(v => v.ErrorMessage)),
                Result = validationResults.All(v => v.Result)
            };

            return totalResult;
        }

        /// <summary>
        /// Получить спарсенное нормализированное выражение 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private List<string> GetNormalizeParseExpression(string expression)
        {
            var expressionElements = _normalizeExpressionService.ParseExpressionToArray(expression);
            expressionElements = _normalizeExpressionService.GetNormalizeExpressionElements(expressionElements);

            return expressionElements;
        }
    }
}
