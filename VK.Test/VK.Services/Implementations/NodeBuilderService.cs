﻿using System.Collections.Generic;
using VK.Services.Implementations.NodeExpression;
using VK.Services.Interfaces;
using VK.Services.Interfaces.NodeExpression;

namespace VK.Services.Implementations
{
    /// <summary>
    /// Сервис сборки дерева выражения
    /// </summary>
    public class NodeBuilderService : INodeBuilderService
    {
        private Dictionary<int, int> _memoryPairBracket;
        public NodeBuilderService()
        {
            _memoryPairBracket = new Dictionary<int, int>();
        }

        /// <summary>
        /// Строю дерево узлов выражения
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public INodeExpression BuildNodeTreeExpression(List<string> expression)
        {
            // Строю пары индексов отрывающихся и закрывающихся скобок
            BuildMemoryPairBracket(expression);
            return BuildNodeTreeExpression(expression, 0, expression.Count-1);
        }

        /// <summary>
        /// Рекурсивный метод, разбиваю на блоки скобок, и собираю по отдельности. endInd включительно в диапазон
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="startInd"></param>
        /// <param name="endInd"></param>
        /// <returns></returns>
        private BracketNodeExpression BuildNodeTreeExpression(List<string> expression, int startInd, int endInd)
        {
            int i = startInd;

            //Список всех узлов, в текущем блоке, без групировки по приоритету
            var listAllNodes = new List<INodeExpression>();

            while (i <= endInd)
            {
                var element = expression[i];

                //Может быть сдвинут индекс на закрывающуюся скобку
                i = CheckCurrentElementExpression(expression, element, i, listAllNodes);
                i++;
            }

            var listNodesFirstPriority = GroupByMultiplyAndDivide(listAllNodes);

            var bracketNodeExpression = GroupByPlusAndMinus(listNodesFirstPriority);

            return bracketNodeExpression;
        }

        /// <summary>
        /// Групирую по 2 приоритету. Групировка по + и -
        /// </summary>
        /// <param name="listNodesFirstPriority"></param>
        /// <returns></returns>
        private BracketNodeExpression GroupByPlusAndMinus(List<INodeExpression> listNodesFirstPriority)
        {
            var listNodesSecondPriority = new List<INodeExpression>();
            var i = 0;
            while (i < listNodesFirstPriority.Count)
            {
                var node = listNodesFirstPriority[i];
                if (node is IOperatorNodeExpressionSecondPriority operatorNode)
                {
                    //Я знаю что у меня всегда с 2х строн от оператора 2 приоритета находятся другие ноды
                    operatorNode.SetPairNodes(listNodesSecondPriority[^1], listNodesFirstPriority[i + 1]);
                    listNodesSecondPriority[^1] = operatorNode;
                    i++;
                }
                else
                {
                    listNodesSecondPriority.Add(node);
                }

                i++;
            }

            //По итогу у меня получится в листе всего 1 нод
            var bracketNodeExpression = new BracketNodeExpression(listNodesSecondPriority[0]);

            return bracketNodeExpression;
        }


        /// <summary>
        /// Групирую по 1 приоритету. Групировка по * и /
        /// </summary>
        /// <param name="listAllNodes"></param>
        /// <returns></returns>
        private List<INodeExpression> GroupByMultiplyAndDivide(List<INodeExpression> listAllNodes)
        {
            var listNodesFirstPriority = new List<INodeExpression>();
            var i = 0;
            while (i < listAllNodes.Count)
            {
                var node = listAllNodes[i];
                if (node is IOperatorNodeExpressionFirstPriority operatorNode)
                {
                    //Я знаю что у меня всегда с 2х строн от оператора 1 приоритета находятся другие ноды
                    operatorNode.SetPairNodes(listNodesFirstPriority[^1], listAllNodes[i + 1]);
                    listNodesFirstPriority[^1] = operatorNode;
                    i++;
                }
                else
                {
                    listNodesFirstPriority.Add(node);
                }

                i++;
            }

            return listNodesFirstPriority;
        }

        /// <summary>
        /// Проверяю текущий элемент выражения
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="element"></param>
        /// <param name="i"></param>
        /// <param name="listNodes"></param>
        /// <returns></returns>
        private int CheckCurrentElementExpression(List<string> expression, string element, int i, List<INodeExpression> listNodes)
        {
            //Если закрывашка, то просто игнорю её
            CheckCloseBracket(element);

            //Если открывашка, получить node для всей скобки, и сдвинуть i на позицию закрывающей
            i = CheckOpenBracket(expression, element, i, listNodes);

            CheckPlus(element, listNodes);

            CheckMinus(element, listNodes);

            CheckMultiply(element, listNodes);

            CheckDivide(element, listNodes);

            CheckNumber(element, listNodes);

            return i;
        }

        /// <summary>
        /// Проверка на число
        /// </summary>
        /// <param name="element"></param>
        /// <param name="listNodes"></param>
        private void CheckNumber(string element, List<INodeExpression> listNodes)
        {
            if (double.TryParse(element, out double number))
            {
                var numberNode = new NumberNodeExpression(number);
                listNodes.Add(numberNode);
            }
        }

        /// <summary>
        /// Проверка на /
        /// </summary>
        /// <param name="element"></param>
        /// <param name="listNodes"></param>
        private void CheckDivide(string element, List<INodeExpression> listNodes)
        {
            if (element == "/")
            {
                var divideNode = new DivideOperatorNodeExpression();
                listNodes.Add(divideNode);
            }
        }

        /// <summary>
        /// Проверка на *
        /// </summary>
        /// <param name="element"></param>
        /// <param name="listNodes"></param>
        private void CheckMultiply(string element, List<INodeExpression> listNodes)
        {
            if (element == "*")
            {
                var multiplyNode = new MultiplyOperatorNodeExpression();
                listNodes.Add(multiplyNode);
            }
        }

        /// <summary>
        /// Провека на )
        /// Если закрывашка, то просто игнорю её
        /// </summary>
        /// <param name="element"></param>
        private void CheckCloseBracket(string element)
        {
            if (element == ")")
            {
                //Просто игнорю, но этот блок оставляю, для целостности структуры
            }
        }

        /// <summary>
        /// Проверка на -
        /// </summary>
        /// <param name="element"></param>
        /// <param name="listNodes"></param>
        private void CheckMinus(string element, List<INodeExpression> listNodes)
        {
            if (element == "-")
            {
                var minusNode = new MinusOperatorNodeExpression();
                listNodes.Add(minusNode);
            }
        }

        /// <summary>
        /// Проверка на +
        /// </summary>
        /// <param name="element"></param>
        /// <param name="listNodes"></param>
        private void CheckPlus(string element, List<INodeExpression> listNodes)
        {
            if (element == "+")
            {
                var plusNode = new PlusOperatorNodeExpression();
                listNodes.Add(plusNode);
            }
        }

        /// <summary>
        /// Проверка на (
        /// Если открывашка, получить node для всей скобки, и сдвинуть i на позицию закрывающей
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="element"></param>
        /// <param name="i"></param>
        /// <param name="listNodes"></param>
        /// <returns></returns>
        private int CheckOpenBracket(List<string> expression, string element, int i, List<INodeExpression> listNodes)
        {
            if (element == "(")
            {
                var closeInd = _memoryPairBracket[i];
                var bracketNode = BuildNodeTreeExpression(expression, i+1, closeInd-1);
                listNodes.Add(bracketNode);
                i = closeInd;
            }

            return i;
        }

        /// <summary>
        /// Строю пары индексов отрывающихся и закрывающихся скобок
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="startInd"></param>
        /// <returns></returns>
        private void BuildMemoryPairBracket(List<string> expression)
        {
            var stackBrackets = new Stack<int>();
            for(int i=0; i < expression.Count; i++)
            {
                var element = expression[i];
                if (element == "(")
                {
                    stackBrackets.Push(i);
                }

                if (element == ")")
                {
                    var startIndFromStack = stackBrackets.Pop();
                    _memoryPairBracket[startIndFromStack] = i;
                }
            }
        }
    }
}
