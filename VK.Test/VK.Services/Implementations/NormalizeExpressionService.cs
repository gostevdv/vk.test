﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using VK.Services.Constants;
using VK.Services.Interfaces;

namespace VK.Services.Implementations
{
    public class NormalizeExpressionService : INormalizeExpressionService
    {
        /// <summary>
        /// Парсинг элементов выражения в массив
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public List<string> ParseExpressionToArray(string expression)
        {
            expression = ConvertByMathMap(expression);

            var pattern = "(?<!\\d)|(?!\\d)";
            return Regex.Split(expression, pattern)
                .Where(s => s != string.Empty)
                .ToList();
        }

        /// <summary>
        /// Преобразую пары +- -+ -- ++ в нормальные - и +
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private string ConvertByMathMap(string expression)
        {
            return Const.MathMapConvert
                .Aggregate(expression, (current, map) => current.Replace(map.Key, map.Value));
        }


        /// <summary>
        /// Получить нормализованное выражение(все отрицательные значения поместить в скобки и добавить как будто вычитание из 0)
        /// И тоже самое с внезапными +. оборачиваем в скобки и сумируем с 0
        /// </summary>
        /// <returns></returns>
        public List<string> GetNormalizeExpressionElements(List<string> expression)
        {
            var normalizeExpression = ScanAndNormalizeExpression(expression);

            return normalizeExpression;
        }

        /// <summary>
        /// Иду по всем элементам выражения и добавляю (0-..), (0+..). 
        /// </summary>
        /// <param name="expression"></param>
        private List<string> ScanAndNormalizeExpression(List<string> expression)
        {
            var normalizeExpression = new List<string>();

            //пихаем true и false при символе "(", true означает что при соответствующей закрывающей ")" необходимо будет добавить еще одну ")", чтоб соответсвовать (0-(..)), (0+(..)) 
            var stackBrackets = new Stack<bool>();
            var i = 0;

            while (i < expression.Count)
            {
                var element = expression[i];

                //Если символ "-" или "+" является первым, либо перед символом "-" или "+" было не число и не закрывающаяся скобка
                if (Const.OperatorsSecondPriority.Contains(element) && (i == 0 || (!double.TryParse(expression[i - 1], out _) && expression[i - 1] != ")")))
                    i = ProcessingForPlusOrMinusForNormalize(expression, i, normalizeExpression, stackBrackets);
                else
                    i = ProcessingForOtherElements(expression, i, normalizeExpression, stackBrackets);
            }

            return normalizeExpression;
        }
        /// <summary>
        /// Обработчик для обычных ситуаций, возвращаю новую позицию i
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="i"></param>
        /// <param name="normalizeExpression"></param>
        /// <param name="stackBrackets"></param>
        /// <returns></returns>
        private int ProcessingForOtherElements(List<string> expression, int i, List<string> normalizeExpression, Stack<bool> stackBrackets)
        {
            var element = expression[i];

            //Проверяю на одиночные цифры. К ним добавляю 0+...
            CheckAloneNumber(expression, i, normalizeExpression);

            normalizeExpression.Add(element);

            //эта открывающая скобка уже без связки с минусом или плюсом, необходимо для поддержания скобочной структуры
            if (element == "(")
            {
                stackBrackets.Push(false);
            }

            if (element == ")")
            {
                //Ситуации что если пришла закрывающая а стек пустой не будет, мы это проверили при валидации, можно не боятся
                //Вытаскиваю соответстующую скобку из стека
                var isNeedNormalize = stackBrackets.Pop();
                //Если вылезка скобка добавленная при свзяке с "-"или"+", знач надо донормализовать выражение
                if (isNeedNormalize)
                {
                    normalizeExpression.Add(")");
                }
            }

            i++;
            return i;
        }

        /// <summary>
        /// Проверяю на одиночные цифры. К ним добавляю 0+...
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="i"></param>
        /// <param name="normalizeExpression"></param>
        private void CheckAloneNumber(List<string> expression, int i, List<string> normalizeExpression)
        {
            if (double.TryParse(expression[i], out _))
            {
                //Если число окружено скобками либо в выражение просто всего 1 число
                if (i > 0 && i < expression.Count - 1 && expression[i - 1] == "(" && expression[i + 1] == ")"
                    || expression.Count == 1)
                {
                    normalizeExpression.Add("0");
                    normalizeExpression.Add("+");
                }
            }
        }

        /// <summary>
        /// Обработчик для + и -, возвращаю новую позицию i
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="i"></param>
        /// <param name="normalizeExpression"></param>
        /// <param name="stackBrackets"></param>
        /// <returns></returns>
        private int ProcessingForPlusOrMinusForNormalize(List<string> expression, int i, List<string> normalizeExpression, Stack<bool> stackBrackets)
        {
            var element = expression[i];

            normalizeExpression.Add("(");
            normalizeExpression.Add("0");

            normalizeExpression.Add(element);

            //"-" и "+" не могут находится на последнем месте, т.к. в валидациях мы такой вариант исключили, значит можно посмотреть на позицию +1, следующий элемент после "-"или "+"

            normalizeExpression.Add(expression[i + 1]); //добавяляю следующий элемент

            if (expression[i + 1] == "(")
            {
                stackBrackets.Push(true);
            }
            else //иначе это число, ибо другие варианты исключили при валидации
            {
                normalizeExpression.Add(")");
            }

            i += 2; //сдвиг на 2 позиции за текущий элемент - или +,и за следующий

            return i;
        }
    }
}
