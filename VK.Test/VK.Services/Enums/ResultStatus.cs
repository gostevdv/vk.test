﻿namespace VK.Services.Enums
{
    public enum ResultStatus
    {
        Ok,
        ValidateError,
        DivideByZeroError,
        UnknownError
    }
}