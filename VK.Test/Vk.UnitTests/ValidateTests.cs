﻿using Microsoft.Extensions.DependencyInjection;
using VK.Services.Implementations;
using VK.Services.Interfaces;
using Xunit;

namespace Vk.UnitTests
{
    public class ValidateTests
    {

        private readonly IValidateExpressionService _validateExpressionService;

        public ValidateTests()
        {
            var services = new ServiceCollection();
            services.AddScoped<IValidateExpressionService, ValidateExpressionService>();

            var serviceProvider = services.BuildServiceProvider();

            _validateExpressionService = serviceProvider.GetService<IValidateExpressionService>();
        }

        /// <summary>
        /// Проверка валидации. Проверка на не пустоту. Ожидаю положительный результат
        /// </summary>
        [Theory]
        [InlineData("(12)+(12)")]
        public void TestNoEmptyExpression_CorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckNoEmpty(expression);

            Assert.True(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на не пустоту. Ожидаю отрицательный результат
        /// </summary>
        [Theory]
        [InlineData("")]
        public void TestNoEmptyExpression_IncorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckNoEmpty(expression);

            Assert.False(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на максимальную длину. Ожидаю положительный результат
        /// </summary>
        [Theory]
        [InlineData("(12)+(12)")]
        public void TestMaxLenExpression_CorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckMaxLen(expression);

            Assert.True(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на максимальную длину. Ожидаю отрицательный результат
        /// </summary>
        [Theory]
        [InlineData("(12)+(12)+(12)+(12)+(12)+(12)+(12)")]
        public void TestMaxLenExpression_IncorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckMaxLen(expression);

            Assert.False(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на корретные символы. Ожидаю положительный результат
        /// </summary>
        [Theory]
        [InlineData("(12)+(12)")]
        public void TestCorrectSymbolsExpression_CorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckCorrectSymbols(expression);

            Assert.True(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на корретные символы. Ожидаю отрицательный результат
        /// </summary>
        [Theory]
        [InlineData("jds293-4")]
        public void TestCorrectSymbolsExpression_IncorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckCorrectSymbols(expression);

            Assert.False(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на корректность использования самих символов друг с другом. Ожидаю положительный результат
        /// </summary>
        [Theory]
        [InlineData("1*-2")]
        [InlineData("1+(-2)")]
        [InlineData("1+(-(2))")]
        [InlineData("1+-2")]
        [InlineData("1-+2")]
        public void TestCheckOrderSignsAndNumbersExpression_CorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckOrderSignsAndNumbers(expression);

            Assert.True(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на корректность последовательность скобок. Ожидаю положительный результат
        /// </summary>
        [Theory]
        [InlineData("1+(-(2))")]
        public void TestCheckSequenceOfBrackets_CorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckSequenceOfBrackets(expression);

            Assert.True(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на корректность последовательность скобок. Ожидаю отрицательный результат
        /// </summary>
        [Theory]
        [InlineData(")(((")]
        [InlineData("(((")]
        [InlineData("((())))")]
        public void TestCheckSequenceOfBrackets_IncorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckSequenceOfBrackets(expression);

            Assert.False(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на первый символ, чтобы не было операторов кроме + и -. Ожидаю положительный результат
        /// </summary>
        [Theory]
        [InlineData("-1+(-(2))")]
        [InlineData("+1+(-(2))")]
        public void TestCheckStartSign_CorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckStartSign(expression);

            Assert.True(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на первый символ, чтобы не было операторов кроме + и -. Ожидаю отрицательный результат
        /// </summary>
        [Theory]
        [InlineData("*1+(-(2))")]
        [InlineData("/1+(-(2))")]
        public void TestCheckStartSign_IncorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckStartSign(expression);

            Assert.False(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на последний символ, чтобы не было операторов. Ожидаю положительный результат
        /// </summary>
        [Theory]
        [InlineData("1+(-(2))")]
        public void TestCheckLastSign_CorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckLastSign(expression);

            Assert.True(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на последний символ, чтобы не было операторов. Ожидаю отрицательный результат
        /// </summary>
        [Theory]
        [InlineData("1+(-(2))+")]
        [InlineData("1+(-(2))-")]
        [InlineData("1+(-(2))*")]
        [InlineData("1+(-(2))/")]
        public void TestCheckLastSign_IncorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckLastSign(expression);

            Assert.False(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на несколько оперторов подряд +-*/(не больше 2х). Ожидаю положительный результат
        /// </summary>
        [Theory]
        [InlineData("1+(-(2))")]
        public void TestCheckConsecutiveOperators_CorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckConsecutiveOperators(expression);

            Assert.True(res.Result);
        }

        /// <summary>
        /// Проверка валидации. Проверка на несколько оперторов подряд +-*/(не больше 2х). Ожидаю отрицательный результат
        /// </summary>
        [Theory]
        [InlineData("1+-+(2))")]
        public void TestCheckConsecutiveOperators_IncorrectResult(string expression)
        {
            var res = _validateExpressionService.CheckConsecutiveOperators(expression);

            Assert.False(res.Result);
        }
    }
}
