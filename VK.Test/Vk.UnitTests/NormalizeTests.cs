﻿using Microsoft.Extensions.DependencyInjection;
using VK.Services.Implementations;
using VK.Services.Interfaces;
using Xunit;

namespace Vk.UnitTests
{
    public class NormalizeTests
    {
        private readonly INormalizeExpressionService _normalizeExpressionExpressionService;

        public NormalizeTests()
        {
            var services = new ServiceCollection();
            services.AddScoped<INormalizeExpressionService, NormalizeExpressionService>();

            var serviceProvider = services.BuildServiceProvider();

            _normalizeExpressionExpressionService = serviceProvider.GetService<INormalizeExpressionService>();
        }

        /// <summary>
        /// Тестирую на парс выражения в массив
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="resCountElementExpression"></param>
        [Theory]
        [InlineData("55-(-(43))", 8)]
        [InlineData("5+5", 3)]
        [InlineData("532*-4", 4)]
        public void TestParseExpressionToArray(string expression, int resCountElementExpression)
        {
            var res = _normalizeExpressionExpressionService.ParseExpressionToArray(expression);

            Assert.Equal(resCountElementExpression, res.Count);
        }

        /// <summary>
        /// Тестирую нормализацию после парса
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="resExpression"></param>
        [Theory]
        [InlineData("-4", "(0-4)")]
        [InlineData("-(5+5)", "(0-(5+5))")]
        [InlineData("5-(5+5)", "5-(5+5)")]
        [InlineData("5-(-(4))", "5-((0-(0+4)))")]
        [InlineData("5*-4", "5*(0-4)")]
        [InlineData("5--4", "5+4")]
        [InlineData("5-+4", "5-4")]
        [InlineData("5-(+(4))", "5-((0+(0+4)))")]
        [InlineData("5", "0+5")]
        [InlineData("--1", "(0+1)")]
        [InlineData("--(1+1)+1", "(0+(1+1))+1")]
        public void TestNormalizeExpressionElements(string expression, string resExpression)
        {
            var elementArray = _normalizeExpressionExpressionService.ParseExpressionToArray(expression);
            var res = _normalizeExpressionExpressionService.GetNormalizeExpressionElements(elementArray);

            Assert.Equal(resExpression, string.Join("", res));
        }
    }
}