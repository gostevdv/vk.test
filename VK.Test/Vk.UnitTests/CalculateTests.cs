﻿using Microsoft.Extensions.DependencyInjection;
using VK.Services.Enums;
using VK.Services.Implementations;
using VK.Services.Interfaces;
using Xunit;

namespace Vk.UnitTests
{
    public class CalculateTests
    {
        private readonly ICalculatorService _calculatorService;

        public CalculateTests()
        {
            var services = new ServiceCollection();
            services.AddScoped<ICalculatorService, CalculatorService>();
            services.AddScoped<IValidateExpressionService, ValidateExpressionService>();
            services.AddScoped<INormalizeExpressionService, NormalizeExpressionService>();
            services.AddScoped<INodeBuilderService, NodeBuilderService>();

            var serviceProvider = services.BuildServiceProvider();

            _calculatorService = serviceProvider.GetService<ICalculatorService>();
        }

        /// <summary>
        /// Тестирую вычисления
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="result"></param>
        [Theory]
        [InlineData("5+5", "10")]
        [InlineData("5+5*5", "30")]
        [InlineData("-5+5*5", "20")]
        [InlineData("-(5+5)*5", "-50")]
        [InlineData("-(5+5)/5", "-2")]
        [InlineData("5*(-4+1)+6", "-9")]
        [InlineData("--1", "1")]
        [InlineData("--(1+1)+1", "3")]
        public void TestCalculateExpression_PositiveResult(string expression, string result)
        {
            var res = _calculatorService.Calculate(expression);

            Assert.Equal(result, res.Result.Answer);
        }

        /// <summary>
        /// Тестирую вычисления, деление на 0
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="resultStatus"></param>
        [Theory]
        [InlineData("5/0", ResultStatus.DivideByZeroError)]
        [InlineData("5/(5-5)", ResultStatus.DivideByZeroError)]
        public void TestCalculateExpression_DivZero(string expression, ResultStatus resultStatus)
        {
            var res = _calculatorService.Calculate(expression);

            Assert.Equal(resultStatus, res.Result.ResultStatusCode);
        }
    }
}