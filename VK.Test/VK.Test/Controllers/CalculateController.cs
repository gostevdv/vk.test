﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using VK.Services.Enums;
using VK.Services.Interfaces;

namespace VK.Test.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class CalculateController : ControllerBase
    {
        private readonly ICalculatorService _calculatorService;

        public CalculateController(ICalculatorService calculatorService)
        {
            _calculatorService = calculatorService;
        }

        /// <summary>
        /// Метод вычисления математического выражения
        /// </summary>
        /// <param name="expression">На вход принимается строка длиной не более 10 символов, которая может содержать цифры и следующие символы: ‘(’, ‘)’, ‘+’, ‘-’, ‘*’, ‘/’.
        /// Выражение записывается в кавычках, пример "1+2".
        /// Сами кавычки не учитываются</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Run([FromBody] string expression = null)
        {
            var result = await _calculatorService.Calculate(expression);

            return Ok(result.ResultStatusCode == ResultStatus.Ok ? result.Answer : result.ErrorMessage);
        }
    }
}
